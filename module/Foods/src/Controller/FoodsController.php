<?php

namespace Foods\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;



class FoodsController extends AbstractActionController{

    private $table;
    public function __construct(FoodsTable $table)
    {
        $this->table = $table;
    }
    public function indexAction()
    {
        $foods = $this->table->fetchAll();
        echo "<pre>";
        print_r($foods);
        echo "</pre>";
        return false;
    }

    public function addAction()
    {
    }

    public function editAction()
    {
    }

    public function deleteAction()
    {
    }
}